
#include "lj_obj.h"

#if LJ_HASJIT

#include "lj_err.h"
#include "lj_str.h"
#include "lj_tab.h"
#include "lj_meta.h"
#include "lj_frame.h"
#if LJ_HASFFI
#include "lj_ctype.h"
#endif
#include "lj_bc.h"
#include "lj_ff.h"
#include "lj_ir.h"
#include "lj_jit.h"
#include "lj_ircall.h"
#include "lj_iropt.h"
#include "lj_trace.h"
#include "lj_record.h"
#include "lj_ffrecord.h"
#include "lj_snap.h"
#include "lj_dispatch.h"
#include "lj_vm.h"
#include "lj_target.h"

#include <stdio.h>

void lj_debug_gcr(lua_State *L, GCobj *gc);

char *bcnames[] = {
#if 0
    /* luajit 2.1 */
    "BC_ISLT", "BC_ISGE", "BC_ISLE", "BC_ISGT", "BC_ISEQV", "BC_ISNEV", "BC_ISEQS", "BC_ISNES", "BC_ISEQN", "BC_ISNEN",
    "BC_ISEQP", "BC_ISNEP", "BC_ISTC", "BC_ISFC", "BC_IST", "BC_ISF", "BC_ISTYPE", "BC_ISNUM", "BC_MOV", "BC_NOT",
    "BC_UNM", "BC_LEN", "BC_ADDVN", "BC_SUBVN", "BC_MULVN", "BC_DIVVN", "BC_MODVN", "BC_ADDNV", "BC_SUBNV", "BC_MULNV",
    "BC_DIVNV", "BC_MODNV", "BC_ADDVV", "BC_SUBVV", "BC_MULVV", "BC_DIVVV", "BC_MODVV", "BC_POW", "BC_CAT", "BC_KSTR",
    "BC_KCDATA", "BC_KSHORT", "BC_KNUM", "BC_KPRI", "BC_KNIL", "BC_UGET", "BC_USETV", "BC_USETS", "BC_USETN", "BC_USETP",
    "BC_UCLO", "BC_FNEW", "BC_TNEW", "BC_TDUP", "BC_GGET", "BC_GSET", "BC_TGETV", "BC_TGETS", "BC_TGETB", "BC_TGETR",
    "BC_TSETV", "BC_TSETS", "BC_TSETB", "BC_TSETM", "BC_TSETR", "BC_CALLM", "BC_CALL", "BC_CALLMT", "BC_CALLT", "BC_ITERC",
    "BC_ITERN", "BC_VARG", "BC_ISNEXT", "BC_RETM", "BC_RET", "BC_RET0", "BC_RET1", "BC_FORI", "BC_JFORI", "BC_FORL",
    "BC_IFORL", "BC_JFORL", "BC_ITERL", "BC_IITERL", "BC_JITERL", "BC_LOOP", "BC_ILOOP", "BC_JLOOP", "BC_JMP", "BC_FUNCF",
    "BC_IFUNCF", "BC_JFUNCF", "BC_FUNCV", "BC_IFUNCV", "BC_JFUNCV", "BC_FUNCC", "BC_FUNCCW", "BC__MAX"
#else
    "BC_ISLT", "BC_ISGE", "BC_ISLE", "BC_ISGT", "BC_ISEQV", "BC_ISNEV", "BC_ISEQS", "BC_ISNES", "BC_ISEQN", "BC_ISNEN",
    "BC_ISEQP", "BC_ISNEP", "BC_ISTC", "BC_ISFC", "BC_IST", "BC_ISF", "BC_MOV", "BC_NOT", "BC_UNM", "BC_LEN",
    "BC_ADDVN", "BC_SUBVN", "BC_MULVN", "BC_DIVVN", "BC_MODVN", "BC_ADDNV", "BC_SUBNV", "BC_MULNV", "BC_DIVNV", "BC_MODNV",
    "BC_ADDVV", "BC_SUBVV", "BC_MULVV", "BC_DIVVV", "BC_MODVV", "BC_POW", "BC_CAT", "BC_KSTR", "BC_KCDATA", "BC_KSHORT",
    "BC_KNUM", "BC_KPRI", "BC_KNIL", "BC_UGET", "BC_USETV", "BC_USETS", "BC_USETN", "BC_USETP", "BC_UCLO", "BC_FNEW",
    "BC_TNEW", "BC_TDUP", "BC_GGET", "BC_GSET", "BC_TGETV", "BC_TGETS", "BC_TGETB", "BC_TSETV", "BC_TSETS", "BC_TSETB",
    "BC_TSETM", "BC_CALLM", "BC_CALL", "BC_CALLMT", "BC_CALLT", "BC_ITERC", "BC_ITERN", "BC_VARG", "BC_ISNEXT", "BC_RETM",
    "BC_RET", "BC_RET0", "BC_RET1", "BC_FORI", "BC_JFORI", "BC_FORL", "BC_IFORL", "BC_JFORL", "BC_ITERL", "BC_IITERL",
    "BC_JITERL", "BC_LOOP", "BC_ILOOP", "BC_JLOOP", "BC_JMP", "BC_FUNCF", "BC_IFUNCF", "BC_JFUNCF", "BC_FUNCV", "BC_IFUNCV",
    "BC_JFUNCV", "BC_FUNCC", "BC_FUNCCW", "BC__MAX"
#endif
};

char *bcm_names[] = {
  "BCMnone", "BCMdst", "BCMbase", "BCMvar", "BCMrbase", "BCMuv",  /* Mode A must be <= 7 */
  "BCMlit", "BCMlits", "BCMpri", "BCMnum", "BCMstr", "BCMtab", "BCMfunc", "BCMjump", "BCMcdata",
  "BCM_max"
};

char *lj_bcm_name(int m)
{
    if (m < 0 || m >= BCM_max)
        return "unknown";
    return bcm_names[m];
}

void unknown_dbg()
{
    /* nothing to do, only for debugging breakpoint */
}

char *lj_bc_name(int bc)
{
    char *rc;
    if (bc < 0 || bc >= (sizeof(bcnames)/sizeof(bcnames[0]))) {
        rc = "unknown";
        unknown_dbg();
    } else {
        rc = bcnames[bc];
    }
    return rc;
}

char *lj_bcname_str(const BCIns *pc)
{
    return lj_bc_name(bc_op(*pc));
}

char *lj_bcname(jit_State *J)
{
    return lj_bc_name(bc_op(*J->pc));
}

char *lj_asmfunction_name(ASMFunction f)
{
#define XX(sym) if (f == sym) return ""#sym;
    XX(lj_vm_record)
    XX(lj_vm_inshook)
    XX(lj_vm_rethook)
    XX(lj_vm_callhook)
#undef XX

#define XX(BCN) if (f == makeasmfunc(lj_bc_ofs[BCN])) return "lj_"#BCN;
    XX(BC_LOOP);
    XX(BC_ILOOP);
    XX(BC_JLOOP);
    XX(BC_FORL);
    XX(BC_IFORL);
    XX(BC_JFORL);
    XX(BC_FORI);
    XX(BC_JFORI);
#undef XX

    return "sym_unknown";
}

char *lj_trace_state_name(int state)
{
    static char *state_name[] = {
        "TRACE_IDLE",    /* 0x0 */ "0x1", "0x2", "0x3", "0x4", "0x5", "0x6",
                                   "0x7", "0x8", "0x9", "0xa", "0xb", "0xc",
                                   "0xd", "0xe", "0xf",
        "TRACE_ACTIVE",  /* 0x10 */
        "TRACE_RECORD",
        "TRACE_START",
        "TRACE_END",
        "TRACE_ASM",
        "TRACE_ERR"
    };
    if (state == 0 || (state >= 0x10 && state <= 0x15))
        return state_name[state];
    return "TRACE_UNKOWN";
}

void lj_debug_dispatch(jit_State *J, BCIns *pc, int dynamic)
{
    char *bcn, *nbcn, *fn, *trn, *prefix;
    void *fnp;
    int op;
    global_State *g;
    ASMFunction *disp;

#ifndef LJ_DD_DISPATCH
    return;
#endif

    /* dont run in __gc call or vmevent */
    if (J2G(J)->hookmask & (HOOK_GC|HOOK_VMEVENT)) {
        return;
    }

    op = bc_op(*pc);

    /* get ASMFunction name */
    g = J2G(J);
    disp = G2GG(g)->dispatch;
    trn = lj_trace_state_name(J->state);                /* get trace state name */
    bcn = lj_bcname_str(pc);                            /* get bytecode name */
    nbcn = lj_bcname_str(pc+1);                         /* get bytecode name of next op */

    if (dynamic) {
        if (op == BC_FORL) {
            /* use next PC of BC_FORL to caculate HotCount */
            uint32_t index = (((uint32_t)(pc + 1) >> 1) & HOTCOUNT_PCMASK) / sizeof(HotCount);
            HotCount hc = G2GG(g)->hotcount[index];
            printf("--- HotCount[BC_FORL:%u] = %u\n", index, hc);
        }
        fnp = disp[op];                 /* get dynamic dispatch */
        if (dynamic == 2) {             /* call dispatch */
            prefix = "dyncall";
        } else {
            prefix = "dynamic";
        }
    } else {
        prefix = "static";
        fnp = disp[GG_LEN_DDISP + op]; /* get static dispatch */
    }

    fn = lj_asmfunction_name(fnp);

    printf("--- %8s dispatch: %-10s:%p:%08x (%-14s:%p) %s:%d -> %-10s\n",
           prefix, bcn, pc, *(uint32_t *)pc, fn, fnp, trn, J->framedepth, nbcn);

    if (op == BC_CALL && J->L) {
        GCfunc *fn = funcV(&J->L->base[bc_a(*pc)]);
        if (fn) {
            BCIns *fpc = mref(fn->c.pc, BCIns *);
            printf("--- call-function f:%p PC:%s\n", fn->c.f, fpc ? lj_bcname_str(fpc) : "nil");
        }
    }

    if (dynamic == 0) printf("\n");
}

static char *irnames[] = {
#if 1
    "LT", "GE", "LE", "GT", "ULT", "UGE", "ULE", "UGT", "EQ", "NE",
    "ABC", "RETF", "NOP", "BASE", "PVAL", "GCSTEP", "HIOP", "LOOP", "USE", "PHI",
    "RENAME", "KPRI", "KINT", "KGC", "KPTR", "KKPTR", "KNULL", "KNUM", "KINT64", "KSLOT",
    "BNOT", "BSWAP", "BAND", "BOR", "BXOR", "BSHL", "BSHR", "BSAR", "BROL", "BROR",
    "ADD", "SUB", "MUL", "DIV", "MOD", "POW", "NEG", "ABS", "ATAN2", "LDEXP",
    "MIN", "MAX", "FPMATH", "ADDOV", "SUBOV", "MULOV", "AREF", "HREFK", "HREF", "NEWREF",
    "UREFO", "UREFC", "FREF", "STRREF", "ALOAD", "HLOAD", "ULOAD", "FLOAD", "XLOAD", "SLOAD",
    "VLOAD", "ASTORE", "HSTORE", "USTORE", "FSTORE", "XSTORE", "SNEW", "XSNEW", "TNEW", "TDUP",
    "CNEW", "CNEWI", "TBAR", "OBAR", "XBAR", "CONV", "TOBIT", "TOSTR", "STRTO", "CALLN",
    "CALLL", "CALLS", "CALLXS", "CARG",
#else
    /* luajit 2.1 */
    "LT", "GE", "LE", "GT", "ULT", "UGE", "ULE", "UGT", "EQ", "NE",
    "ABC", "RETF", "NOP", "BASE", "PVAL", "GCSTEP", "HIOP", "LOOP", "USE", "PHI",
    "RENAME", "PROF", "KPRI", "KINT", "KGC", "KPTR", "KKPTR", "KNULL", "KNUM", "KINT64", "KSLOT",
    "BNOT", "BSWAP", "BAND", "BOR", "BXOR", "BSHL", "BSHR", "BSAR", "BROL", "BROR",
    "ADD", "SUB", "MUL", "DIV", "MOD", "POW", "NEG", "ABS", "ATAN2", "LDEXP",
    "MIN", "MAX", "FPMATH", "ADDOV", "SUBOV", "MULOV", "AREF", "HREFK", "HREF", "NEWREF",
    "UREFO", "UREFC", "FREF", "STRREF", "LREF", "ALOAD", "HLOAD", "ULOAD", "FLOAD", "XLOAD", "SLOAD",
    "VLOAD", "ASTORE", "HSTORE", "USTORE", "FSTORE", "XSTORE", "SNEW", "XSNEW", "TNEW", "TDUP",
    "CNEW", "CNEWI", "BUFHDR", "BUFPUT", "BUFSTR", "TBAR", "OBAR", "XBAR", "CONV", "TOBIT", "TOSTR", "STRTO", "CALLN",
    "CALLA", "CALLL", "CALLS", "CALLXS", "CARG",
#endif
};

#define IRNAMES_LEN (sizeof(irnames) / sizeof(char *))

char *ir_opname(IROp op)
{
    if (op < IRNAMES_LEN)
        return irnames[op];
    else
        return "unknown";
}

static void debug_irm(IRIns *ir)
{
    uint8_t m = lj_ir_mode[ir->o];
    uint8_t k = irm_kind(m);
    printf("\t[IRM: ");
    if (irm_iscomm(m)) printf("Commutative ");
    printf("{ ");
    if (k == IRM_N) printf("Normal/Ref ");
    if (k == IRM_A) printf("Alloc ");
    if (k == IRM_L) printf("Load ");
    if (k == IRM_S) printf("Store ");
    printf("} ");
    if (m & IRM_W) printf("Non-week guard ");
    printf("]\n");
}

static void debug_irt(IRIns *ir)
{
    int has_irt = 0;
#define HAS_IRT(name, size) if (irt_type(ir->t) == IRT_##name) has_irt = 1;
    IRTDEF(HAS_IRT)
#undef HAS_IRT

    if (has_irt) printf("\t[IRT:");

#define PIRT(name, size) if (irt_type(ir->t) == IRT_##name) printf(" "#name);
    IRTDEF(PIRT)
#undef PIRT

    if (has_irt) printf("]");

#if 0
    /* IR Type */
    printf("\t[IRT:");

    #define printflag(flag) if (irt_is##flag(ir->t)) printf(" "#flag);

#if 1   /* highlight */
    if (irt_isguard(ir->t)) printf(" GUARD");
    if (irt_ismarked(ir->t)) printf(" MARK");
    if (irt_isphi(ir->t)) printf(" ISPHI");
#else
    printflag(guard)
    printflag(marked)
    printflag(phi)
#endif
    printflag(nil)
    printflag(pri)
    printflag(lightud)
    printflag(str)
    printflag(tab)
    printflag(cdata)
    printflag(float)
    printflag(num)
    printflag(int)
    printflag(i8)
    printflag(u8)
    printflag(i16)
    printflag(u16)
    printflag(u32)
    printflag(i64)
    printflag(u64)
    printflag(num)
    printflag(addr)

    printf("\n");
#endif
}

void ir_debug_offset(jit_State *J, IRIns *ir, int off)
{
    IROp op;
    IRRef op1, op2;

    // #define IR(ref)			(&as->ir[(ref)])
    op = ir->o;
    op1 = ir->op1;
    op2 = ir->op2;
    // op
    printf("[%04x] %-6s", off, ir_opname(op));
    // op1
    if (op == IR_KINT) {
        printf(" %d", ir->i);
        goto out;
    }
    if (op == IR_KNUM) {
        printf(" %.4g", ir_knum(ir)->n);
        goto out;
    }
    if (op == IR_KGC) {
        printf(" ");
        lj_debug_gcr(J->L, ir_kgc(ir));
        goto out;
    }
    printf(" %04x", (op1 > REF_BIAS) ? (op1 - REF_BIAS) : op1);
    if (op1 >= J->cur.nk && op1 < REF_BIAS) {
        IRIns *ir1 = &J->cur.ir[op1];
        printf("(%s)", ir_opname(ir1->o));
    }
    if (op == IR_SLOAD && op1 == 0) {
        printf("(curfunc)");
    }
    // op2
    if (op == IR_SLOAD) {
#define FP(name) if (op2 & IRSLOAD_##name) { printf(" "#name); }
        FP(PARENT);     // #define IRSLOAD_PARENT          0x01    /* Coalesce with parent trace. */
        FP(FRAME);      // #define IRSLOAD_FRAME           0x02    /* Load hiword of frame. */
        FP(TYPECHECK);  // #define IRSLOAD_TYPECHECK       0x04    /* Needs type check. */
        FP(CONVERT);    // #define IRSLOAD_CONVERT         0x08    /* Number to integer conversion. */
        FP(READONLY);   // #define IRSLOAD_READONLY        0x10    /* Read-only, omit slot store. */
        FP(INHERIT);    // #define IRSLOAD_INHERIT         0x20    /* Inherited by exits/side traces. */
#undef FP

    } else if (op == IR_FLOAD) {
#define FP(name, ofs) if (op2 == IRFL_##name) { printf(" %04x("#ofs"):%x", op2, (unsigned int)ofs); }
IRFLDEF(FP)
#undef FP

    } else if (op == IR_HLOAD) {
        /* right value is unused */

    } else {
        printf(" %04x", (op2 > REF_BIAS) ? (op2 - REF_BIAS) : op2);
        if (op2 >= J->cur.nk && op2 < REF_BIAS) {
            IRIns *ir2 = &J->cur.ir[op2];
            printf("(%s)", ir_opname(ir2->o));
        }
    }
out:

    debug_irt(ir);
    debug_irm(ir);

    printf("\n");
}

void ir_debug(jit_State *J, IRIns *ir)
{
    int off;
    if (ir - J->cur.ir == REF_DROP) {
        printf("[DROP]\n");
        return;
    }
    if ((int)((ir - (IRIns *)J->cur.ir) - REF_BIAS) >= 0) {
        off = (int)((ir - (IRIns *)J->cur.ir) - REF_BIAS);
    } else {
        off = (int)((ir - (IRIns *)J->cur.ir)/* - REF_BIAS*/);
    }
    ir_debug_offset(J, ir, off);
}


void lj_debug_ir(jit_State *J, IRRef ref)
{
    IRIns *ir = &J->cur.ir[ref];
    ir_debug(J, ir);
}

void asm_irk_debug_raw(jit_State *J, IRRef ins)
{
    IRIns *ir;
    printf("  + k:%04x -> %04x\n", J->cur.nk, ins);
    for (IRRef insk = J->cur.nk; insk < ins; insk++) {
        printf("  + ");
        ir = &J->cur.ir[insk];
        ir_debug(J, ir);

#if 0   /* luajit 2.1 */
        if ((ir)->o == IR_KNUM || (ir)->o == IR_KINT64 ||
            (LJ_GC64 &&
             ((ir)->o == IR_KGC ||
              (ir)->o == IR_KPTR || (ir)->o == IR_KKPTR)))
        {
            insk += 1;
        }
#endif

    }
}

void asm_ir_debug_raw2(jit_State *J, IRRef ins)
{
    IRIns *ir;

    for (; ins < J->cur.nins; ins++) {
        printf("  + ");
        ir = &J->cur.ir[ins];
        ir_debug(J, ir);
    }
}

int lua_tv_type(lua_State *L, cTValue *o)
{
    if (tvisnumber(o)) {
        return LUA_TNUMBER;
#if LJ_64
    } else if (tvislightud(o)) {
        return LUA_TLIGHTUSERDATA;
#endif
#if 0   /* lua_type() */
    } else if (o == niltv(L)) {
        return LUA_TNONE;
#else
    } else if (L != NULL && o == niltv(L)) {
        return LUA_TNONE;
    } else if (tvisnil(o)) {
        return LUA_TNIL;
#endif
    } else {  /* Magic internal/external tag conversion. ORDER LJ_T */
        uint32_t t = ~itype(o);
#if LJ_64
        int tt = (int)((U64x(75a06,98042110) >> 4*t) & 15u);
#else
        int tt = (int)(((t < 8 ? 0x98042110u : 0x75a06u) >> 4*(t&7)) & 15u);
#endif
        lua_assert(tt != LUA_TNIL || tvisnil(o));
        return tt;
    }
}

void lj_debug_tv(TValue *o)
{
    if (tvisnumber(o)) {
        printf("%.2f", numberVnum(o));
    } else if (tvisstr(o)) {
        printf("\"%s\"", strVdata(o) /* strdata(strV(o)) */);
    } else {
        printf("<%s>", lua_typename((lua_State *) NULL, lua_tv_type((lua_State *) NULL, o)));
    }
}

void lj_debug_gcr(lua_State *L, GCobj *gc)
{
    TValue o;
    setgcV(L, &o, gc, ~gc->gch.gct);
    lj_debug_tv(&o);
}

void lj_debug_k(lua_State *L, GCproto *pt, int idx)
{
    if (idx < 0) {
        GCobj *gc = proto_kgc(pt, idx);
        lj_debug_gcr(L, gc);

    } else {
        TValue *o = proto_knumtv(pt, idx);
        lj_debug_tv(o);
    }
}

void lj_debug_opcode(lua_State *L, GCproto *pt, const BCIns *pc, int pci)
{
    BCIns ins = *pc;
    BCOp op,ra,rb,rc,rd;
    char *bcn;

//** +----+----+----+----+
//** | B  | C  | A  | OP | Format ABC
//** +----+----+----+----+
//** |    D    | A  | OP | Format AD
//** +--------------------
//** MSB               LSB
    op = bc_op(ins);
    ra = bc_a(ins);
    rb = bc_b(ins);
    rc = bc_c(ins);
    rd = bc_d(ins);
    bcn = lj_bcname_str(pc);

    /* opcode */

    printf("  + %p %04d: %-10s %3d ", pc, pci, bcn, ra);

    /* oprands */

    if (bcmode_b(op) != 0)
        rd = rd & 0xff;

    if (bcmode_c(op) == BCMjump) {
        printf("=> %04d", pci+rd-0x7fff); /* src/jit/bc.lua bcline() */
        goto out;
    }

    switch (bcmode_b(op)) {
        case BCMnone:
            printf("%3d", rd);
            break;  /* Upgrade rc to 'rd'. */
        case BCMvar:
            printf("%3dv %3d", rb, rc);
            break;
        default:
            printf("%3d  %3d", rb, rc);
            break;  /* Handled later. */
    }

    /* comment */

    if (op == BC_CALL) {
        printf("\t; nresults=%d nargs=%d", rb - 1, rc -1);
    }

    switch (bcmode_c(op)) {
        case BCMstr:
            printf("\t; ");
            lj_debug_k(L, pt, -rd-1);
            break;
        case BCMnum:
            printf("\t; ");
            lj_debug_k(L, pt, rd);
            break;
        case BCMfunc:
            printf("\t; ");
            lj_debug_k(L, pt, -rd-1);
            break;
        case BCMuv:
            printf("\t; ");
            //lj_debug_k(L, pt, rd); TODO
            break;
        default:
            break;
    }

out:
    if (pci == 0) printf("\t; Placeholder (A=pt->framesize)");
    printf("\t;%s %s %s\n",
            lj_bcm_name(bcmode_a(op)),
            lj_bcm_name(bcmode_b(op)),
            lj_bcm_name(bcmode_c(op))
            );
}

/* referer to src/lib_jit.c:jit_util_funck */
void lj_debug_func_fnk(lua_State *L, GCproto *pt)
{
    /* < 0 */
    for (int idx = -pt->sizekgc; idx < 0; idx++) {
        printf("  + %4d: ", idx);
        lj_debug_k(L, pt, idx);
        printf("\n");
    }

    /* >= 0 */
    for (int idx = 0; idx < pt->sizekn; idx++) {
        printf("  + %4d: ", idx);
        lj_debug_k(L, pt, idx);
        printf("\n");
    }
}

void lj_debug_func_fn(lua_State *L, GCfunc *fn, GCproto *pt) {
    const BCIns *pc = mref(fn->l.pc, const BCIns);

    for (int i = 0; i < pt->sizebc; i++) {
        lj_debug_opcode(L, pt, &pc[i], i);
    }
}

void lj_debug_fn(lua_State *L, GCfunc *fn)
{
    GCproto *pt;
    if (!isluafunc(fn)) return;
    pt = funcproto(fn);
    printf("  + fn:%p\n", fn);
    printf("  + pt:%p(chunkname:\"%s\" framesize:%d, sizebc:%d, numparams:%d)\n",
           pt,
           proto_chunknamestr(pt), pt->framesize, pt->sizebc, pt->numparams);
    lj_debug_func_fn(L, fn, pt);

    printf("  + const table:%p (sizekn:%d, sizekgc:%d)\n",
           mref(pt->k, void *),
           pt->sizekn, pt->sizekgc);
    lj_debug_func_fnk(L, pt);
}

void lj_debug_func(lua_State *L, TValue *base)
{
    GCfunc *fn;
    if (base == NULL) base = L->base;
    printf("  + base:%p\n", base);
    fn = (&gcref((base-1)->fr.func)->fn);
    if (!isluafunc(fn)) return;
    lj_debug_fn(L, fn);
}

int lua_dd_cfunc(lua_State *L)
{
    lua_Number n = lua_tonumber(L, 1);
    printf("in dd cfunc: %d\n", n);
    return 0;
}

// ---- asm register ---
char *lj_regname(Reg r)
{

#define RETNAME(name) \
    if (r & RID_##name) return ""#name;

    GPRDEF(RETNAME)

    GPRDEF(RETNAME)

    return "unknown reg";
}

#endif
