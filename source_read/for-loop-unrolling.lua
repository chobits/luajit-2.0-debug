--[[
$ ./luajit -jdump=tbirsmxaA -Ohotloop=1 -O-loop for-loop-unrolling.lua
---- TRACE 1 start for-loop-unrolling.lua:3
0007  ADDVV    1   1   0
0008  SUBVN    0   5   0  ; 1
0009  FORL     2 => 0007
---- TRACE 1 IR
....              SNAP   #0   [ ---- ]
0001 rbx      int SLOAD  #3    CI
0002 xmm6  >  num SLOAD  #2    T
0003       >  num SLOAD  #1    T
0004 xmm6     num ADD    0003  0002
0005 rbp   >  int SUBOV  0001  +1
0006 rbx      int ADD    0001  +1
....              SNAP   #1   [ ---- 0005 0004 ]
0007       >  int LE     0006  +100
0008 xmm5     num CONV   0006  num.int
0009 xmm7     num CONV   0005  num.int
....              SNAP   #2   [ ---- 0009 0004 0008 ---- ---- 0008 ]

$./luajit -jdump=tbirsmxaA -Ohotloop=1  for-loop-unrolling.lua
---- TRACE 1 start for-loop-unrolling.lua:3
0007  ADDVV    1   1   0
0008  SUBVN    0   5   0  ; 1
0009  FORL     2 => 0007
---- TRACE 1 IR
....              SNAP   #0   [ ---- ]
0001 rbx      int SLOAD  #3    CI
0002 xmm7  >  num SLOAD  #2    T
0003       >  num SLOAD  #1    T
0004 xmm7   + num ADD    0003  0002
0005 rbp   >+ int SUBOV  0001  +1
0006 rbx    + int ADD    0001  +1
....              SNAP   #1   [ ---- 0005 0004 ]
0007       >  int LE     0006  +100
....              SNAP   #2   [ ---- 0005 0004 0006 ---- ---- 0006 ]
0008 ------------ LOOP ------------
0009 xmm7     num CONV   0005  num.int
0010 xmm7   + num ADD    0009  0004
0011 rbp   >+ int SUBOV  0006  +1
0012 rbx    + int ADD    0006  +1
....              SNAP   #3   [ ---- 0011 0010 ]
0013       >  int LE     0012  +100
0014 rbx      int PHI    0006  0012
0015 xmm7     num PHI    0004  0010
0016 rbp      int PHI    0005  0011
0017 r15      nil RENAME 0005  #2
0018 xmm6     nil RENAME 0004  #2
--]]

local x = 0
local s = 0
for i = 0, 100 do
    s = s + x
    x = i - 1
end
