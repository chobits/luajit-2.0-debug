
local t = {}
local mt = { __call = function (...) print ("meta call: ", ...) end }
setmetatable(t, mt)

t(1)
t(1, 2)
t("hello", "world")
