-- local s = 0
for i = 1, 58 do
    -- s = s + 1
end
return 6

--[[

---- TRACE 1 start IR_for_phi.lua:1
> trace_start() emit:
+ J->cur.ir[8000] IR_BASE  0000 0000
+ J->cur.ir[8001] IR_SLOAD 0001 0028
0005  FORL     0 => 0005
+ record [   5] BC_FORL
> lj_record_ins() emit:
+ J->cur.ir[8002] IR_ADD   0001 7ffb
+ J->cur.ir[8003] IR_LE    0002 7ffc
> lj_opt_loop() emit:
+ J->cur.ir[8004] IR_LOOP  0000 0000
+ J->cur.ir[8005] IR_ADD   0002 7ffb
+ J->cur.ir[8006] IR_LE    0005 7ffc
+ J->cur.ir[8007] IR_PHI   0002 0005

-- BYTECODE -- IR_for_phi.lua:0-42
0001    KSHORT   0   1
0002    KSHORT   1  58
0003    KSHORT   2   1
0004    FORI     0 => 0006
0005 => FORL     0 => 0005
0006 => RET0     0   1

---- TRACE 1 IR
0001    int SLOAD  #1    CI
0002  + int ADD    0001  +1
0003 >  int LE     0002  +58
0004 ------ LOOP ------------
0005  + int ADD    0002  +1
0006 >  int LE     0005  +58
0007    int PHI    0002  0005
---- TRACE 1 mcode 40
f125ffd5  mov dword [0x000424a0], 0x1
f125ffe0  cvtsd2si ebp, [rdx]
f125ffe4  add ebp, +0x01
f125ffe7  cmp ebp, +0x3a
f125ffea  jg 0xf1250014 ->1
->LOOP:
f125fff0  add ebp, +0x01
f125fff3  cmp ebp, +0x3a
f125fff6  jle 0xf125fff0        ->LOOP
f125fff8  jmp 0xf125001c        ->3
---- TRACE 1 stop -> loop

--]]
