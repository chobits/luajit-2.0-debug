local n = 1
local s = 0

for i = 1, 60 do -- loop hot count: 57. +1 for next jit running
    if i == 57 then
        n = n * 2
    end
    s = s + n
end

print(s)

--[[
-- BYTECODE -- IR_phi.lua:0-82
0001    KSHORT   0   1
0002    KSHORT   1   0
0003    KSHORT   2   1
0004    KSHORT   3  68
0005    KSHORT   4   1
0006    FORI     2 => 0012
0007 => ISNEN    5   0      ; 1234
0008    JMP      6 => 0010
0009    MULVN    0   0   1  ; 2            <<< not compiled
0010 => ADDVV    1   1   0
0011    FORL     2 => 0007
0012 => GGET     2   0      ; "print"
0013    MOV      3   1
0014    CALL     2   1   2
0015    RET0     0   1

---- TRACE 1 start IR_phi.lua:4
> trace_start() emit:
+ J->cur.ir[8000] IR_BASE  0000 0000
+ J->cur.ir[8001] IR_SLOAD 0003 0028
0007  ISNEN    5   0      ; 1234
0008  JMP      6 => 0010
+ record: BC_ISNEN
> lj_record_ins() emit:
+ J->cur.ir[8002] IR_NE    0001 7ffa
0010  ADDVV    1   1   0
+ record: BC_ADDVV
> lj_record_ins() emit:
+ J->cur.ir[8003] IR_SLOAD 0002 0004
+ J->cur.ir[8004] IR_SLOAD 0001 0004
+ J->cur.ir[8005] IR_ADD   0004 0003
0011  FORL     2 => 0007
+ record: BC_FORL
> lj_record_ins() emit:
+ J->cur.ir[8006] IR_ADD   0001 7ffb
+ J->cur.ir[8007] IR_LE    0006 7ffc
> lj_opt_loop() emit:
+ J->cur.ir[8008] IR_LOOP  0000 0000
+ J->cur.ir[8009] IR_NE    0006 7ffa
+ J->cur.ir[800a] IR_ADD   0005 0004
+ J->cur.ir[800b] IR_ADD   0006 7ffb
+ J->cur.ir[800c] IR_LE    000b 7ffc
+ J->cur.ir[800d] IR_PHI   0006 000b
+ J->cur.ir[800e] IR_PHI   0005 000a
---- TRACE 1 IR
0001    int SLOAD  #3    CI
0002 >  int NE     0001  +1234
0003 >  num SLOAD  #2    T
0004 >  num SLOAD  #1    T
0005  + num ADD    0004  0003
0006  + int ADD    0001  +1
0007 >  int LE     0006  +68
0008 ------ LOOP ------------
0009 >  int NE     0006  +1234
0010  + num ADD    0005  0004
0011  + int ADD    0006  +1
0012 >  int LE     0011  +68
0013    int PHI    0006  0011
0014    num PHI    0005  0010
---- TRACE 1 mcode 108
f125ff91  mov dword [0x000424a0], 0x1
f125ff9c  cvtsd2si ebp, [rdx+0x10]
f125ffa1  cmp ebp, 0x4d2
f125ffa7  jz 0xf1250014 ->1
f125ffad  cmp dword [rdx+0xc], 0xfffeffff
f125ffb4  jnb 0xf1250018        ->2
f125ffba  movsd xmm7, [rdx+0x8]
f125ffbf  cmp dword [rdx+0x4], 0xfffeffff
f125ffc6  jnb 0xf1250018        ->2
f125ffcc  movsd xmm0, [rdx]
f125ffd0  addsd xmm7, xmm0
f125ffd4  add ebp, +0x01
f125ffd7  cmp ebp, +0x44
f125ffda  jg 0xf125001c ->3
->LOOP:
f125ffe0  cmp ebp, 0x4d2
f125ffe6  jz 0xf1250024 ->5
f125ffec  addsd xmm7, xmm0
f125fff0  add ebp, +0x01
f125fff3  cmp ebp, +0x44
f125fff6  jle 0xf125ffe0        ->LOOP
f125fff8  jmp 0xf1250028        ->6
---- TRACE 1 stop -> loop
]]
