--[[
 + [0002] SLOAD  0001 TYPECHECK [IRT: NUM]      [IRM: { Load } Nwg ]       <<< y
fold fwd_sload: return 8002=J->slot[1]=J->base[0]
  > re-emit (unmodified)
  > old IR:8002
  + SUB: sub[8002] = 8002
--]]

local y = 123456
for i=0,3 do
    i = y
end
