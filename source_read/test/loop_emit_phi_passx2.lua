--[[
> loop_unroll() emit
+ subst[8001, 8007]
 > loop:
 + [0001] SLOAD  0002 INHERIT   [IRT: NUM]      [IRM: { Load } Nwg ]
fold fwd_sload: return 8005=J->slot[2]=J->base[1]  > re-emit (loop-carried)
  > [0005] ADD    0001 7ffb(KNUM)       [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  + SUB: sub[8001] = 8005
  + PHI: phi[0000] = 8005 (loop-carried & potential PHI)

 + [0002] SLOAD  0001 TYPECHECK [IRT: NUM]      [IRM: { Load } Nwg ]
fold fwd_sload: return 8004=J->slot[1]=J->base[0]  > re-emit (loop-carried)
  > [0004] ADD    0003 7ff8(KNUM)       [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  + SUB: sub[8002] = 8004
  + PHI: phi[0001] = 8004 (loop-carried & potential PHI)

 + [0003] ADD    0002 7ff9(KNUM)        [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  - op1 = 8004 = subst[8002]
  > re-emit (loop-out)
  > [0008] ADD    0004 7ff9(KNUM)       [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  + SUB: sub[8003] = 8008

 + [0004] ADD    0003 7ff8(KNUM)        [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  - op1 = 8008 = subst[8003]
  > re-emit (loop-out)
  > [0009] ADD    0008 7ff8(KNUM)       [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  + SUB: sub[8004] = 8009

 + [0005] ADD    0001 7ffb(KNUM)        [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  - op1 = 8005 = subst[8001]
  > re-emit (loop-out)
  > [000a] ADD    0005 7ffb(KNUM)       [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
  + SUB: sub[8005] = 800a

 + [0006] LE     0005 7ffc(KNUM)        [IRT: NUM]      [IRM: { Normal/Ref } Nwg ]
  - op1 = 800a = subst[8005]
  > re-emit (loop-out)
  > [000b] LE     000a 7ffc(KNUM)       [IRT: NUM]      [IRM: { Normal/Ref } Nwg ]
  + SUB: sub[8006] = 800b

 > loop_emit_phi nphi=2 onsnap=3
  + #1 PHI: phi[0000] = 8005
  + #1 PHI: phi[0001] = 8004
  + #1 PHI: passx #2 L:8004,R:8009(recurrences failed)
            lref: [0004] ADD    0003 7ff8(KNUM) [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]
            rref: [0009] ADD    0008 7ff8(KNUM) [IRT: NUM]      [IRM: Commutative { Normal/Ref } Nwg ]


  subst    ---- TRACE 2 start test_for.lua:2
           0006  ADDVN    0   0   0  ; 0.1
           0007  ADDVN    0   0   1  ; 1.1
           0008  FORL     1 => 0006
           ---- TRACE 2 IR
           ....              SNAP   #0   [ ---- ]
  0005     0001 xmm6     num SLOAD  #2    I
  0004     0002 xmm7  >  num SLOAD  #1    T
  0008     0003 xmm7     num ADD    0002  +0.1
  0009     0004 xmm7   + num ADD    0003  +1.1
  0010     0005 xmm6   + num ADD    0001  +1
           ....              SNAP   #1   [ ---- 0004 ]
           0006       >  num LE     0005  +3
           ....              SNAP   #2   [ ---- 0004 0005 ---- ---- 0005 ]
           0007 ------------ LOOP ------------
           0008 xmm7     num ADD    0004  +0.1
           0009 xmm7   + num ADD    0008  +1.1
           0010 xmm6   + num ADD    0005  +1
           ....              SNAP   #3   [ ---- 0009 ]
           0011       >  num LE     0010  +3
           0012 xmm6     num PHI    0005  0010
           0013 xmm7     num PHI    0004  0009
]]
local s = 0
for i=0,3 do
    s = s + 0.1
    s = s + 1.1
end
