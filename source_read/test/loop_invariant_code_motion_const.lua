local const_out = 123456
local s = 0
for i=0,3 do
    local const = const_out + 1.2   -- hoisted Loop-Invariant IR
    s = s + const
end
