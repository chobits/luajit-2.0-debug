


#LUA_PATH='./src/?.lua;;' src/luajit -bl -e 'function f() print(package.path) end' 
#LUA_PATH='./src/?.lua;;' src/luajit -bl source_read/for.lua
#LUA_PATH='./src/?.lua;;' src/luajit -jdump source_read/for.lua
#LUA_PATH='./src/?.lua;;' src/luajit -jbc source_read/IR_for_phi.lua 

#LUA_PATH='./src/?.lua;;' src/luajit -jdump=tbirsmxaA source_read/for.lua 
#LUA_PATH='./src/?.lua;;' src/luajit -jdump=bitmsr source_read/for.lua 

source="source_read/for.lua"
source="source_read/IR_for_phi.lua"
source="source_read/callfunc.lua"
dump="-jdump=tbirsmxaA"
hotloop="-Ohotloop=1"

echo "------------ origin ------------\n"
echo LUA_PATH='./src/?.lua;;' src/luajit $hotloop $source
LUA_PATH='./src/?.lua;;' src/luajit $hotloop $source
echo "------------ ir > asm ------------\n"
echo LUA_PATH='./src/?.lua;;' src/luajit $hotloop $dump $source
LUA_PATH='./src/?.lua;;' src/luajit $hotloop $dump $source
echo "------------ bc ------------\n"
LUA_PATH='./src/?.lua;;' src/luajit -jbc $source
