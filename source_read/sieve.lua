primes = {}
n = 100

for i = 1, n do
    table.insert(primes, true)
end

for i = 2, n do
    if primes[i] then
        for k = 2*i, n, i do    -- 2i,3i,...,n
            primes[k] = false
        end
    end
end

for i = 2, n do
    if primes[i] then
        print(i)
    end
end
